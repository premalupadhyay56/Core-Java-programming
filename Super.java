/*
    created by premal upadhyay
*/
package javaprogramming;
class Box4{
	int height;
	int width;
	int depth;
	
	Box4(){
		this.height = 10;
		this.width = 10;
		this.depth = 10;
	}
	Box4(int height, int width, int depth){
		this.height = height;
		this.width = width;
		this.depth = depth;
	}
	Box4(int len){
		this.height = this.width = this.depth = len;
	}
}
class BoxWeight extends Box4{
	BoxWeight(){
		super(10, 10, 10);
	}
	BoxWeight(int height, int width, int depth){
		super(height, width, depth);
	}
	BoxWeight(int len){
		super(len);
	}
	public void setHeight(int height){
		super.height = height;
	}
	public void setWidth(int width){
		super.width = width;
	}
	public void setDepth(int depth){
		super.depth = depth;
	}
	public int getHeight(){
		return super.height;
	}
	public int getWidth(){
		return super.width;
	}
	public int getDepth(){
		return super.depth;
	}
        /*Note:we can also call method of super class by using super keyword
        Example :- super.methodName(); in sub class...
        */
        
}
public class Super{
	public static void main(String args[]){
		BoxWeight b1 = new BoxWeight();
		dispDetails(b1);
		BoxWeight b2 = new BoxWeight(20);
		dispDetails(b2);
		BoxWeight b3 = new BoxWeight(10, 20, 30);
		dispDetails(b3);
	}
	static void dispDetails(BoxWeight b){
		System.out.println("Box details...");
		System.out.println("Height :- "+b.getHeight());
		System.out.println("Width :- "+b.getWidth());
		System.out.println("Depth :- "+b.getDepth());
	}
	
}