/*
    created by premal upadhyay
*/
package javaprogramming;

class A{
    void disp(){
        System.out.println("In A class");
    }
}
class B extends A{
    void disp(){
        System.out.println("In B class");
    }
}
class C extends A{
    void disp(){
        System.out.println("In C class");
    }
}

public class DynamicMethodDispatch{
    public static void main(String args[]){
        A a = new A();
        B b = new B();
        C c = new C();
        A a1; //only reference created
        a.disp();
        b.disp();
        c.disp();
        a1 = new B();
        a1.disp();
        a1 = b;
        a1.disp();
        a1 = c;
        a1.disp();
        
    }
}