/*
    created by premal upadhyay
*/
package javaprogramming;
import java.util.Scanner;
public class SudokuChecker{
    static boolean sudokuCheck(int s[][]){
        //row wise check
        for(int i = 0 ; i < 9 ; i++){
            for(int j = 0 ; j < 8 ; j++){
                if(s[i][j] == s[i][j+1]){
                    return false;
                }
                else{
                    return true;
                }
            }
        }
        //column wise check
        for(int i = 0 ; i < 9 ; i++){
            for(int j = 0 ; j < 8 ; j++){
                if(s[j][i] == s[j][i+1]){
                    return false;
                }
                else{
                    return true;
                }
            }
        }
        return false;
    }
    public static void main(String args[]){
        int s[][] = new int[9][9];
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the sudoku");
        for(int i = 0 ; i < 9 ; i++){
            for(int j = 0 ; j < 9 ; j++){
                s[i][j] = sc.nextInt();
            }
        }
        if(sudokuCheck(s)){
            System.out.println("Given sudoku is correct..");
        }
        else{
            System.out.println("Given sudoku is not correct..");
        }
    }
}