/*
    created by premal upadhyay
*/
package javaprogramming;

class Box1 {

    int height;
    int width;
    int depth;

    Box1() {
        this.height = 10;
        this.width = 10;
        this.depth = 10;
    }

    void volume() {
        System.out.println("Volume :- " + (this.height * this.width * this.depth));
    }
}

public class Constructor {

    public static void main(String args[]) {
        Box1 b1 = new Box1();
        b1.volume();
    }
}
