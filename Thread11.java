/*
    created by premal upadhyay
*/
package thread;

class Q1 {

    int n;
    boolean valueSet = false;

    synchronized void put(int n) {
        while (valueSet == true) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.n = n;
        valueSet = true;
        System.out.println("Put :- " + n);
        notify();
    }

    synchronized int get() {
        while (valueSet == false) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Got :- " +n);
        valueSet = false;
        notify();
        return n;
    }
}

class Producer1 implements Runnable {

    Q1 q;
    Thread t;

    public Producer1(Q1 q) {
        this.q = q;
        t = new Thread(this, "Producer");
        t.start();
    }

    @Override
    public void run() {
        int n = 0;
        while (true) {
            q.put(n++);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

class Consumer1 implements Runnable{

    Q1 q;
    Thread t;

    public Consumer1(Q1 q) {
        this.q = q;
        t = new Thread(this, "Consumer");
        t.start();
    }

    @Override
    public void run() {
        while (true) {
            q.get();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class Thread11 {

    public static void main(String args[]) {
        Q1 q = new Q1();
        Producer1 t1 = new Producer1(q);
        Consumer1 t2 = new Consumer1(q);
    }
}
