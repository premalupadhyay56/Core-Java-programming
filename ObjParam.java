/*
    created by premal upadhyay
*/
package javaprogramming;

class Box3 {

    int width;
    int height;
    int depth;

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getHeight() {
        return this.height;
    }

    public int getWidth() {
        return this.width;
    }

    public int getDepth() {
        return this.depth;
    }

    public Box3 copy(Box3 obj) {
        Box3 b = new Box3();
        b.depth = obj.getDepth();
        b.height = obj.getHeight();
        b.width = obj.getWidth();
        return b;
    }
}

public class ObjParam {

    public static void main(String args[]) {
        Box3 b1 = new Box3();
        Box3 b2 = new Box3();
        b1.setHeight(10);
        b1.setWidth(20);
        b1.setDepth(30);
        b2 = b1.copy(b1);
        System.out.println("Object 1 height :- " + b1.getHeight());
        System.out.println("Object 1 width :- " + b1.getWidth());
        System.out.println("Object 1 depth :- " + b1.getDepth());
        System.out.println("Object 2 height :- " + b2.getHeight());
        System.out.println("Object 2 width :- " + b2.getWidth());
        System.out.println("Object 2 depth :- " + b2.getDepth());

    }
}
