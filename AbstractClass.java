/*
    created by premal upadhyay
*/
package javaprogramming;

abstract class Model{
    abstract void run();
    abstract void reverse();
    void stop(){
        System.out.println("Car is stop...");
    }
}
abstract class Run extends Model{
    @Override
    void run(){
        System.out.println("Car is running...");
    }
}
class Car extends Run{
    @Override
    void reverse(){
        System.out.println("Car is reverse...");
    }
}
public class AbstractClass{
    public static void main(String args[]){
        Car c = new Car();
        c.run();
        c.reverse();
        c.stop();
    }
}