/*
    created by premal upadhyay
*/
package javaprogramming;

public class NestedTry{
    public static void main(String args[]){
        int a[] = new int[2];
        try{
           
            
            try{
                 int var;
            var = 5 / 0;
                a[5] = 10;
            }
            catch(ArrayIndexOutOfBoundsException ae){
                ae.printStackTrace();
            }
            finally{
                System.out.println("Finally block of inner try...");
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally{
            System.out.println("Finally block of outer try...");
        }
    }
}