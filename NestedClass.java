/*
    created by premal upadhyay
*/
package javaprogramming;
/*
*Note: An inner class can be static but can access only static method of outer class
*Syntax: class outter_class{
            static class inner_class{
            }
        }
*/
class Test{
    int i = 10;
    void display(){
        System.out.println("Outer class...");
    }
    class Test1{
        void display1(){
            System.out.println("Inner class...");
            display();
        }
    }
}
public class NestedClass{
    
    public static void main(String args[]){
        Test.Test1 t2 = new Test().new Test1();
        t2.display1();  
    }
}