/*
    created by premal upadhyay
*/
//interthread communication...
package thread;
class Q{
    int n;
    synchronized void put(int n){
        this.n = n;
        System.out.println("Put :- "+this.n);
    }
    synchronized int get(){
        System.out.println("Got :- "+this.n);
        return this.n;
    }   
}
class Producer implements Runnable{
    Q q;
    Thread t;
    Producer(Q q){
        this.q = q;
        t = new Thread(this, "Producer");
    }
    
    @Override
    public void run(){
        int n = 0;
        while(true){
           q.put(n++);
        }
    }
}
class Consumer implements Runnable{
    Q q;
    Thread t;
    Consumer(Q q){
        this.q = q;
        t = new Thread(this);
    }
    @Override
    public void run(){
        while(true){
            q.get();
        }
    }
}
public class Thread10{
    public static void main(String args[]){
        Q q = new Q();
        Producer t1 = new Producer(q);
        Consumer t2 = new Consumer(q);
        t1.t.start();
        t2.t.start();
        try{
            t1.t.join();
            t2.t.join();
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}