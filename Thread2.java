/*
    created by premal upadhyay
*/
//implement runnable interface
package thread;

public class Thread2 implements Runnable{
    public static void main(String args[]){
        Thread t1 = new Thread(new Thread2());
        Thread t2 = new Thread(new Thread2());
        t1.start();
        t2.start();
    }

    @Override
    public void run(){
        for(int i = 0 ; i < 100 ; i++){
            System.out.println(i);
        }
    }
}