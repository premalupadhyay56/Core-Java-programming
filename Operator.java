/*
    created by premal upadhyay
*/
package javaprogramming;

public class Operator{
    public static void main(String args[]){
        int a = 10;
        int b = 5;
        
        //arithmetic operator
        System.out.println(a+b);
        System.out.println(a-b);
        System.out.println(a*b);
        System.out.println(a/b);
        
        //Relational operator
        System.out.println(a>b);
        System.out.println(a<b);
        System.out.println(a==b);
        System.out.println(a!=b);
        System.out.println(a>=b);
        System.out.println(a<=b);
        
        //logical operator
        System.out.println(a==10 && b==5);
        System.out.println(a==10 || b==5);
        System.out.println(!(a==b));
        
        //bitwise operator
        System.out.println(a&b);
        System.out.println(a^b);

        //ternary operator
        System.out.println((a>b)?a:b);
        
        //increment and decrement opearator or unary operator
        System.out.println(++a);
        System.out.println(++b);
        
        //assignment operator
        System.out.println(a+=10);
        System.out.println(b-=10);
        System.out.println(a*=10);
        System.out.println(b*=10);
        
        
    }
}