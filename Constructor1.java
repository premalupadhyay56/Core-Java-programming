/*
    created by premal upadhyay
*/
package javaprogramming;

class Box2{
    int height, width, depth;
   static int temp;
   int objNo;
    Box2(){
        objNo = ++temp;
        this.height = 10;
        this.width = 10;
        this.depth = 10;
    }
    Box2(int height, int width, int depth){
        objNo = ++temp;
        this.height = height;
        this.width = width;
        this.depth = depth;
    }
    public void volume(){
        System.out.println("Volume for object "+objNo+":- "+(this.height * this.width * this.depth));
    }
}
public class Constructor1{
    public static void main(String args[]){
      Box2 b1 = new Box2();
      Box2 b2 = new Box2(10, 20, 30);
      b1.volume();
      b2.volume();
    }
}