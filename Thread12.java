/*
    created by premal upadhyay
*/
package thread;

class A{
    synchronized void dispA(B b){
        System.out.println("Aquired lock on A class...");
        b.dispB(this);
    }
    
}
class B{
    synchronized void dispB(A a){
        System.out.println("Acquired lock on B class...");
        a.dispA(this);
    }
    
}
class ThreadLocker implements Runnable{
    A a;
    B b;
    Thread t;
    ThreadLocker(A a, B b){
        this.a = a;
        this.b = b;
        t = new Thread(this, "thread1");
    }
    @Override
    public void run(){
        a.dispA(b);
        System.out.println("by thread 1");
        try{
            Thread.sleep(1000);
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
        b.dispB(a);
    }
}

class ThreadLocker1 implements Runnable{
    A a;
    B b;
    Thread t;
    ThreadLocker1(A a, B b){
        this.a = a;
        this.b = b;
        t = new Thread(this, "thread2");
    }
    @Override
    public void run(){
        b.dispB(a);
        System.out.println("by thread 2");
        try{
            Thread.sleep(1000);
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
        a.dispA(b);
    }
}
public class Thread12{
    public static void main(String args[]){
        A a = new A();
        B b = new B();
        ThreadLocker p1 = new ThreadLocker(a, b);
        ThreadLocker1 p2 = new ThreadLocker1(a, b);
        p1.t.start();
        p2.t.start();
        try{
           p1.t.join();
           p2.t.join();
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}