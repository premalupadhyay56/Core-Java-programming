/*
    created by premal upadhyay
*/
package javaprogramming;

public class MethodOverload{
    
    void sum(int a, int b){
        System.out.println("Addition :- "+(a+b));
    }
    void sum(){
        System.out.println("Addition :- "+(5+6));
    }
    int sum(int a){
        return a+6;
    }
    int sum(double x, int y){
        return (int) (x+y);
    }
    public static void main(String args[]){
        MethodOverload m1 = new MethodOverload();
        m1.sum();
        m1.sum(5,6);
        System.out.println("Addition :- "+m1.sum(5));
        System.out.println("Addition :- "+m1.sum(5.5,6));
    }
}