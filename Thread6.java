/*
    created by premal upadhyay
*/
package thread;
/*
program:isAlive() and join() method example
*/
class Test1 implements Runnable {
    String threadname;
    Thread t;
    public Test1(String name){
        threadname = name;
        t = new Thread(this, threadname);
    }
    @Override
    public void run(){
        for(int i = 0 ; i < 5 ; i++){
            System.out.println(threadname+ " "+ i);
            try{
                Thread.sleep(1000);
            }
            catch(InterruptedException e){
                System.out.println(e);
            }
        }
        System.out.println(t.getName()+" exiting");
    }
}

public class Thread6 {

    public static void main(String args[]) {
        Test1 t1 = new Test1("Thread1");
        Test1 t2 = new Test1("Thread2");
        t1.t.start();
        t2.t.start();
        //waiting for two threads to complete
       System.out.println("Thread1 running "+t1.t.isAlive());
       System.out.println("Thread2 running "+t2.t.isAlive());
        try{
            t1.t.join();
            t2.t.join();
             
        }
        catch(InterruptedException e){
            System.out.println(e);
        }
        System.out.println("Thread1 running "+t1.t.isAlive());
        System.out.println("Thread2 running "+t2.t.isAlive());
        System.out.println("Main thread exiting");
    }
}
