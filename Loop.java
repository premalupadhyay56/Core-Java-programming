/*
    created by premal upadhyay
*/
package javaprogramming;

public class Loop{
    public static void main(String args[]){
        int i=0;
        int a[] = {1,2,3,4,5,6,7,8,9};
        while(i<10){
            System.out.println("I am in while loop..");
            i++;
        }
        i = 0;
        do{
            
            System.out.println("I am in do while loop..");
            i++;
        }
        while(i<10);
        
        for(i = 0 ; i < 10 ; i++){
            System.out.println("I am in for loop..");
        }
        
        //for each loop or itration loop
        for(int a1 : a){
            System.out.println(a1);
        }
        //break statement
        for(i = 0 ; i < 100 ; i++){
            if(i == 25)
                break;
            System.out.println(i);
        }
        
        //countinue statement
        for(i = 0 ; i < 100 ; i++){
            if(i == 25)
               continue;
            System.out.println(i);
        }
    }
}