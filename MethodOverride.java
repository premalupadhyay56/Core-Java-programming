/*
    created by premal upadhyay
*/
package javaprogramming;

class Figure {

    double dim1, dim2;

    Figure(double dim1, double dim2) {
        this.dim1 = dim1;
        this.dim2 = dim2;
    }

    public double area() {
        System.out.println("Figure counld not have area...");
        return 0;
    }
}

class Rectangle extends Figure {

    Rectangle(double dim1, double dim2) {
        super(dim1, dim2);
    }

    @Override
    public double area() {
        return dim1 * dim2;
    }
}

class Triangle extends Figure {

    Triangle(double dim1, double dim2) {
        super(dim1, dim2);
    }

    @Override
    public double area() {
        return (dim1 * dim2) / 2;
    }
}

public class MethodOverride {

    public static void main(String args[]) {
        Figure f = new Figure(10, 20);
        System.out.println("Area of figure :- " + f.area());
        f = new Rectangle(20, 40);
        System.out.println("Area of rectangle :- " + f.area());
        f = new Triangle(30, 60);
        System.out.println("Area of triangle :- " + f.area());
    }
}
