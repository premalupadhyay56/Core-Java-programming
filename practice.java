/*
    created by premal upadhyay
*/
package javaprogramming;

public class practice{
	class InnerDemo{
		public void disp(){
			System.out.println("Inner class method..");
		}
	}
	public void disp(){
		System.out.println("Outer class method..");
	}
	public static void main(String args[]){
		practice.InnerDemo objInner = new practice().new InnerDemo();
		objInner.disp();
		practice objOuter = new practice();
		objOuter.disp();
	}
}