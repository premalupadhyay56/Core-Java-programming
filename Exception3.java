/*
    created by premal upadhyay
*/
package Exception;

public class Exception3{
    public static void main(String args[]){
        int a[] = new int[5];
        for(int i = 0 ; i <= 5 ; i++){
           try{
                a[i] = 12 / 0;
           }
           catch(ArithmeticException ae){
               ae.printStackTrace();
           }
           catch(ArrayIndexOutOfBoundsException a1){
               a1.printStackTrace();
           }
        }
    }
}