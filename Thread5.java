/*
    created by premal upadhyay
*/
package thread;

class Test implements Runnable {

    String threadname;
    Thread t;

    Test(String name) {
        threadname = name;
        t = new Thread(this, threadname);
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println(threadname + " " + i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }
        System.out.println(threadname + " thread exiting");
    }
}

public class Thread5 {

    public static void main(String args[]) throws InterruptedException {
        Test t1 = new Test("Thread1");
        Test t2 = new Test("Thread2");
        t1.t.start();
        t2.t.start();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + " thread exiting");
    }
}
