/*
    created by premal upadhyay
*/
package collection;

import java.util.ArrayList;

public class ArrayList1 {

    public static void main(String args[]) {
        ArrayList animals = new ArrayList();
        animals.add("Cow");
        animals.add("Dog");
        animals.add("Cat");
        animals.add(0, "Tiger");
        System.out.println("Array list is empty status :- " + animals.isEmpty());
        for (int i = 0; i < animals.size(); i++) {
            System.out.println(animals.get(i));
        }
        for (Object animal : animals) {
            System.out.println("Animal :- " + animal);
        }

    }
}
