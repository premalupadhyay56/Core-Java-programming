/*
    created by premal upadhyay
*/
package javaprogramming;

class Student1{
    int id;
    String name;
    Student1(int id, String name){
        this.id = id;
        this.name = name;
    }
    public void setId(int id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public int getId(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }
}
class Sports1 extends Student1{
    int marks;

    Sports1(int id, String name, int marks) {
        super(id, name);
        this.marks = marks;
    }
    public void setMarks(int marks){
        this.marks = marks;
    }
    public int getMarks(){
        return this.marks;
    }
}
class Academic1 extends Student1{
    int marks;

    Academic1(int id, String name, int marks) {
        super(id, name);
        this.marks = marks;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }
    
}
public class Hierarchical{
    public static void main(String args[]){
        Sports1 s = new Sports1(101, "Premal", 30);
        Academic1 a = new Academic1(101, "Premal", 45);
        System.out.println("Student id :- "+s.getId());
        System.out.println("Student name :- "+s.getName());
        System.out.println("Sports Marks :- "+s.getMarks());
        System.out.println("Academic Marks :- "+a.getMarks());
    }
}