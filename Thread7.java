/*
    created by premal upadhyay
*/
//thread priority program
package thread;

public class Thread7 extends Thread{
    public Thread7(String name){
        super(name);
    }
    @Override
    public void run(){
        for(int i = 0 ; i < 30 ; i++){
            System.out.println(i);
            try{
                Thread.sleep(1000);
            }
            catch(InterruptedException e){
                e.printStackTrace();
            }
        }
    }
    public static void main(String args[]){
        Thread t;
        t = Thread.currentThread();
        t.setName("MyThread1");
        Thread7 t1 = new Thread7("MyThread2");
        Thread7 t2 = new Thread7("MyThread3");
        t1.setPriority(Thread.MIN_PRIORITY);
        t2.setPriority(Thread.MAX_PRIORITY);
        t1.start();
        t2.start();
        try{
            t1.sleep(1000);
            t2.sleep(1000);
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}