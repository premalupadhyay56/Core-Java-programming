/*
    created by premal upadhyay
*/
//Thread synchronization
package thread;

class CallMe{
    synchronized void call(String msg){
        System.out.print("["+msg);
        try{
            Thread.sleep(1000);
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
        System.out.print("]");
    }
}
class Caller implements Runnable{
   CallMe targ;
   String msg;
   Thread t;
   public Caller(CallMe target, String msg){
       targ = target;
       this.msg = msg;
       t = new Thread(this);
   }
    @Override
    public void run(){
        targ.call(msg);
    }
}
public class Thread8{
    public static void main(String args[]){
        CallMe target = new CallMe();
        Caller t1 = new Caller(target, "Hello");
        Caller t2 = new Caller(target, "Synchronized");
        Caller t3 = new Caller(target, "world");
        t1.t.start();
        t2.t.start();
        t3.t.start();
        try{
            t1.t.join();
            t2.t.join();
            t3.t.join();
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}