/*
    created by premal upadhyay
*/
package javaprogramming;

class Student{
    private int id;
    private String name;

    Student(int id, String name){
        this.id = id;
        this.name = name;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
class Exam extends Student{
    int test1;
    int test2;

    public Exam(int id, String name, int test1, int test2) {
        super(id, name);
        this.test1 = test1;
        this.test2 = test2;
    }

    public int getTest1() {
        return test1;
    }

    public void setTest1(int test1) {
        this.test1 = test1;
    }

    public int getTest2() {
        return test2;
    }

    public void setTest2(int test2) {
        this.test2 = test2;
    }
    
}
class Sports extends Exam{
    int sportsMarks;

    public Sports(int id, String name, int test1, int test2, int sportsMarks) {
        super(id, name, test1, test2);
        this.sportsMarks = sportsMarks;
    }
    

    public int getSportsMarks() {
        return sportsMarks;
    }

    public void setSportsMarks(int sportsMarks) {
        this.sportsMarks = sportsMarks;
    }
}
public class MultiLevel{
    static void dispStudent(Sports s){
        System.out.println("Id :- "+s.getId());
        System.out.println("Name :- "+s.getName());
        System.out.println("Test 1 marks :- "+s.getTest1());
        System.out.println("Test 2 marks :- "+s.getTest2());
        System.out.println("Sports Marks :- "+s.getSportsMarks());
    }
    public static void main(String args[]){
        Sports s = new Sports(101, "premal", 50, 60, 80);
        dispStudent(s);
    }
}