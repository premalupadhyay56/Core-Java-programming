/*
    created by premal upadhyay
*/
package javaprogramming;

interface i1{
    void disp();
}

class ci1 implements i1{
    public void disp(){
        System.out.println("Accesing interface 1 through class 1");
    }
}
class ci2 implements i1{
    public void disp(){
        System.out.println("Accesing interface 1 through class 2");
    }
}
public class Interface{
    public static void main(String args[]){
        ci1 c1 = new ci1();
        c1.disp();
        ci2 c2 = new ci2();
        c2.disp();
    }
}