/*
    created by premal upadhyay
*/
package collection;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayList2 {

    public static void main(String args[]) {
        ArrayList employee = new ArrayList();
        Scanner sc = new Scanner(System.in);
        String name;
        employee.add("premal");
        employee.add("pujan");
        employee.add("darsh");
        employee.add("smit");
        System.out.println("Enter the number of employee to get employee information...");
        int no = sc.nextInt();
        try {
            System.out.println("Employee no : " + no + " Employee name : " + employee.get(no - 1));
        } catch (Exception ex) {
            System.out.println("Employee n3ot at that position..");
        }
        System.out.println("Before removing the employee..");
        for (Object emp : employee) {
            System.out.println(emp);
        }
        System.out.println("Enter the name you want to remove :- ");
        name = sc.next();
        if (employee.contains(name)) {
            employee.remove(name);
        }
        System.out.println("After removing employee..");
        for (Object employees : employee) {
            System.out.println(employees);
        }
        System.out.println("Size :- " + employee.size());
    }
}
