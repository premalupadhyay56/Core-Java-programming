/*
    created by premal upadhyay
*/
package javaprogramming;

final class Test2{
    final static double pi = 3.14;
    public final static double areaOfCircle(int r){
        return (pi*r*r);
    }
}
public class Final{
    public static void main(String args[]){
      Test2 obj = new Test2();
      System.out.println("Area of circle :- "+Test2.areaOfCircle(50));
    }
}