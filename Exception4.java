
/*
    created by premal upadhyay
*/
//throw keyword
package Exception;
public class Exception4{
    public static void main(String args[]){
		Exception4 obj = new Exception4();
		try{
			obj.dispData();
		}
		catch(ArithmeticException ae){
			ae.printStackTrace();
		}
                finally{
                    System.out.println("Finally block");
                }
    }
	public void dispData(){
		for(int i = 0 ; i < 5 ; i++){
                    	System.out.println(i);
			if(i > 3){
				throw new ArithmeticException();
			}
		}	
	}
}