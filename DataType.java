/*
    created by premal upadhyay
*/
package javaprogramming;
public class DataType{
	public static void main(String args[]){
		int a = 10;//4 byte
		float f = 3.5f;//4 byte
		double d = 3.5000;//variable //8 byte
		char c = 'c';//2 byte
		byte b = 12;//1 byte
		short s = 23;//2 byte
		long l = 25000;//8 byte
		String str = "string";
		System.out.println("Integer :- "+a);
		System.out.println("Byte :- "+b);
		System.out.println("Short :- "+s);
		System.out.println("Long :- "+l);
		System.out.println("Double :- "+d);
		System.out.println("Character :- "+c);
		System.out.println("String :- "+str);
	}
}