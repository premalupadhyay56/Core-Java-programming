/*
    created by premal upadhyay
*/
//chaining exception
package Exception;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Exception6{
    public static void method1(){
        try{
            method2();
        }
        catch(ArithmeticException a){
            a.printStackTrace();
            throw new ArithmeticException();
        }
    }
    public static void method2(){
        throw new ArithmeticException();
    }
    public static void main(String args[]){
        try{
            method1();
        }
        catch(ArithmeticException a){
            a.printStackTrace();
        }
    }
}