/*
    created by premal upadhyay
*/
//controlling main thread
package thread;

public class Thread1 {

    public static void main(String args[]) {
        Thread t = Thread.currentThread();
        t.setName("My thread");
        try {
            for (int i = 0; i < 5; i++) {
                System.out.println(i);
                Thread.sleep(500);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
