/*
    created by premal upadhyay
*/
package thread;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
public class Thread4 extends Thread{
        static int a[] = new int[50];
        static int b[] = new int[50];
        static int c[] = new int[100];
    public static void main(String args[]) throws InterruptedException{
        Thread4 t = new Thread4("Thread 1");
        Thread4 t1 = new Thread4("Thread 2");
        int i, j=0;
        t.start();
        t1.start();
       
        if(t.isAlive() && t1.isAlive()){
            for(i = 0 ; i < 50 ; i++){
                System.out.println(a[i]);
            }
            for(i = 0 ; i < 50 ; i++){
                System.out.println(b[i]);
            }
        }
        else{
            
        }
    }
    public Thread4(String name){
        super(name);
    }
    @Override
    public void run(){
        
        Random r = new Random();
        if(Thread.currentThread().getName().equals("Thread 1")){
            for(int i = 0 ; i < 50 ; i++){
                a[i] = (i + 26);
                System.out.println(Thread.currentThread().getName());
            }
            for(int i = 0 ; i < 50 - 1 ; i++){
                for(int j = 0 ; j < 50 ; j++){
                    if(a[i] < a[j]){
                        int temp = a[i];
                        a[i] = a[j];
                        a[j] = temp;
                    }
                }
            }
            for(int i = 0 ; i < 50 ; i++){
                System.out.println(a[i]);
            }
        }  
        if(Thread.currentThread().getName().equals("Thread 2")){
            for(int i = 0 ; i < 50 ; i++){
                b[i] = ThreadLocalRandom.current().nextInt(50, 100);
                System.out.println(Thread.currentThread().getName());
            }
            for(int i = 0 ; i < 50 - 1 ; i++){
                for(int j = 0 ; j < 50 ; j++){
                    if(b[i] > b[j]){
                        int temp = b[i];
                        b[i] = b[j];
                        b[j] = temp;
                    }
                }
            }
        } 
    }
}