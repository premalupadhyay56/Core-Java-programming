/*
    created by premal upadhyay
*/
package javaprogramming;

class Parent { //parent, super, Base

    int i, j;

    public void showIj() {
        System.out.println(i + " " + j);
    }
}

public class Inheritance extends Parent { //Child, Sub, Derived

    int k;

    public void showK() {
        System.out.println(k);
    }

    public void sum() {
        System.out.println("Sum of :- " + i + " " + j + " and " + k + " = " + (i + j + k));
    }

    public static void main(String args[]) {
        Inheritance i = new Inheritance();
        i.i = 10;
        i.j = 20;
        i.k = 30;
        i.showIj();
        i.showK();
        i.sum();
    }
}
