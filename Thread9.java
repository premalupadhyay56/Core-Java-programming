/*
    created by premal upadhyay
*/
//synchronized method
package thread;

class Caller1 implements Runnable{
    CallMe targ;
    String msg;
    Thread t;
    public Caller1(CallMe target, String msg){
        this.targ = target;
        this.msg = msg;
        t = new Thread(this);
    }
    @Override
    public void run(){
        synchronized(targ){
            targ.call(this.msg);
        }
    }
}
public class Thread9{
    public static void main(String args[]){
        CallMe c = new CallMe();
        Caller1 t1 = new Caller1(c,"Hello");
        Caller1 t2 = new Caller1(c, "world");
        Caller1 t3 = new Caller1(c, "synchronized");
        t1.t.start();
        t2.t.start();
        t3.t.start();
        try{
            t1.t.join();
            t2.t.join();
            t3.t.join();
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
    }
}