/*
    created by premal upadhyay
*/
package practice;
class Q{
    int n;
    
    synchronized void put(int n){
        this.n = n;
        System.out.println("Put :- "+this.n);
    }
    synchronized void get(){
        System.out.println("Got :- "+this.n);
    }
}
class Producer implements Runnable{
    Q q;
    Thread t;
    Producer(Q q){
        this.q = q;
        t = new Thread(this, "producer");
    }
    @Override
    public void run(){
        
    }
}
public class Practice1{
    public static void main(String args[]){
    
    }
}