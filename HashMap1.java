/*
    created by premal upadhyay
*/
package collection;

import java.util.Scanner;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HashMap1 {

    public static void main(String args[]) {
        HashMap<Integer, String> hashMap = new HashMap();
        int key;
        String value;
        Scanner sc = new Scanner(System.in);
        System.out.println("Note that key must be in number and value is in character...");
        System.out.println("Enter the number of entries :- ");
        int no = sc.nextInt();
        for (int i = 0; i < no; i++) {
            System.out.println("Enter the key :- ");
            key = sc.nextInt();
            System.out.println("Enter the value :- ");
            value = sc.next();
            hashMap.put(key, value);
        }
        //Itrator class to itrat hashmap data structure
        Iterator iterator = hashMap.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry pair = (Map.Entry) iterator.next();
            System.out.println("Key :- " + pair.getKey() + " Value :- " + pair.getValue());
        }
    }
}
