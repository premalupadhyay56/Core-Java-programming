/*
    created by premal upadhyay
*/
package javaprogramming;

class Box{
    int height;
    int width;
    int depth;
    void volume(){
        System.out.println("Volume :- "+(this.height * this.width * this.depth));
    }
    void setHeight(int height){
        this.height = height;
    }
    void setWidth(int width){
        this.width = width;
    }
    void setDepth(int depth){
        this.depth = depth;
    }
    int getHeight(){
        return this.height;
    }
    int getWidth(){
        return this.width;
    }
    int getDepth(){
        return this.depth;
    }
}
public class Class1{
    public static void main(String args[]){
       Box b1 = new Box();
       b1.setHeight(30);
       b1.setWidth(20);
       b1.setDepth(30);
       System.out.println("Height :- "+b1.getHeight());
       System.out.println("Width :- "+b1.getWidth());
       System.out.println("Depth :- "+b1.getDepth());
       b1.volume();
    }
}