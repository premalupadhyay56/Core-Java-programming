/*
    created by premal upadhyay
*/
package javaprogramming;

interface i12{
    interface i2{
        void disp();
    }
    void disp();
}
class Implementer implements i12,i12.i2{
    @Override
    public void disp(){
        System.out.println("In interface.....");
    }
}
public class NestedInterface{
    public static void main(String args[]){
        Implementer.i2 i1 = new Implementer();
        i1.disp();
    }
}