/*
    created by premal upadhyay
*/
package javaprogramming;

public class Switch{
    public static void main(String args[]){
        int a = 3;
        
        switch(a){
            case 1:
                System.out.println("Your choice is 1");
                break;
            case 2:
                System.out.println("Your choice is 2");
                break;
            case 3:
                System.out.println("Your choice is 3");
                break;
            default:
                System.out.println("Enter the valid choice...");
        }
    }
}