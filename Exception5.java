/*
    created by premal upadhyay
*/
package Exception;

import java.util.logging.Level;
import java.util.logging.Logger;

class Test1 extends Exception{
    String err;
    Test1(String err){
        super(err);
        this.err = err;
    }
    @Override
    public String toString(){
        return "Test1Exception"+err;
    }
}
public class Exception5{
    public static void main(String args[]){
        Exception5 obj = new Exception5();
        obj.ExcMethod();
    }
    public void ExcMethod(){
        int counter;
        for(counter = 0 ; counter < 100 ; counter++){
            if(counter > 25){
                try {
                    throw new Test1("Counter overflow..");
                } catch (Test1 ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}