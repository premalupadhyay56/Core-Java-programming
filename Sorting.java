/*
    created by premal upadhyay
*/
package javaprogramming;
import java.util.Scanner;
public class Sorting{
	public static void main(String args[]){
		int a[] = new int[5];
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the five element..");
		for(int i = 0 ; i < 5 ; i++){
			a[i] = sc.nextInt();
		}
		System.out.println("Before sort array is..");
		for(int i = 0 ; i < a.length ; i++){
			System.out.println(a[i]);
		}
		for(int i = 0 ; i < a.length-1 ; i++){
			for(int j = i+1 ; j < a.length ; j++){
				if(a[i] > a[j]){
					int temp = a[i];
					a[i] = a[j];
					a[j] = temp;
				}
			}
		}
		System.out.println("After sort array is..");
		for(int i = 0 ; i < a.length ; i++){
			System.out.println(a[i]);
		}
	}
}