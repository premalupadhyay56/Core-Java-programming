/*
    created by premal upadhyay
*/
package thread;

public class Thread3 extends Thread{
    public Thread3(String s){
        super(s);
    }
    public static void main(String args[]) throws InterruptedException{
        Thread3 t1 = new Thread3("My thread1");
        Thread3 t2 = new Thread3("My thread2");
        System.out.println(t1.getName());
        System.out.println(t2.getName());
        t1.start();
        t1.join();
        t2.start();
    }
    @Override
    public void run(){
        for(int i = 0 ; i < 100 ; i++){
            System.out.println(i);
            
        }
    }
}